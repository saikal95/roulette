import {Component, Input, OnInit} from '@angular/core';
import {RouletteService} from "./shared/roulette.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'roulette';
  numb!: number;
  numbArray!: number[];
  current! : string;


  constructor(private rouletteService: RouletteService) {}
  ngOnInit(): void {
    this.numbArray = this.rouletteService.getArray();
    this.rouletteService.newNumber.subscribe((numb: number) => {
      this.numb = numb;
      this.numbArray.push(this.numb);
      this.current = this.rouletteService.getColor(this.numb);
    })
  }


  returnNumb() {
    this.rouletteService.start();

  }


  stopNumb(){
    this.rouletteService.stop();
  }

  resetInterval(){
    this.rouletteService.reset();

  }


  getClass(){
    if(this.current == 'red'){
    } else if(this.current == 'black'){
    }
    console.log(this.current);
    return  this.current;
  }



}
