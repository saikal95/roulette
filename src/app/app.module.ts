import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {RouletteService} from "./shared/roulette.service";
import {ColorDirectives} from "./directives/color-directives";

@NgModule({
  declarations: [
    AppComponent,
    ColorDirectives
  ],
  imports: [
    BrowserModule,
  ],
  providers: [RouletteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
