import {EventEmitter, Injectable, Input} from "@angular/core";
@Injectable()

export class RouletteService {

  newNumber = new EventEmitter<number>();
  private numb: number = 1;
  id! : any;
 @Input()  numberArray: number[] = [];

  getArray(){
    return this.numberArray.slice();
  }

  generateNumber() {
    return Math.round(Math.random() * (36));

  }

  start() {
    this.id = setInterval(() => {
      this.numb = this.generateNumber();
     this.newNumber.emit(this.numb);
    }, 1000);

  }

  stop(){
    return clearInterval(this.id);
  }

  reset(){
   this.numberArray.splice(0, this.numberArray.length);
   console.log(this.numberArray);
  }




  getColor(number:number){
    console.log(number);
    let value: string = '';
    if(number >= 1 && number <= 10 && number % 2 === 0 || number >= 19 && number <= 28 && number % 2 === 0 ) {
      value = 'black';
    } else  if (number >= 1 && number <= 10 && number % 2 === 1 || number >= 19 && number <= 28 && number % 2 === 1 ){
      value = 'red';
    } else if(number >= 11 && number <= 18 && number % 2 === 0 || number >= 29 && number <= 36 && number % 2 === 0 ){
       value = 'red';
    } else if(number >= 11 && number <= 18 && number % 2 === 1 || number >= 29 && number <= 36 && number % 2 === 1){
      value = 'black';
    } else if(number === 0){
      value = 'zero'
    } else {
      value = 'unknown';
    }
    return value;
  }


}
